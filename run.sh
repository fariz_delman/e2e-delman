#!/usr/bin/bash

set -eux

NAMESPACE=$(openssl rand -hex 6)
JWT_SECRET=$(openssl rand -hex 12)

VERSION=$1
FRONTEND_IMAGE=$2:$VERSION
BACKEND_IMAGE=$3:$VERSION

ENDPOINT=$VERSION.fariz.delman.io

docker network create $NAMESPACE

docker network connect $NAMESPACE openresty

docker run -d --network $NAMESPACE --name fe-$NAMESPACE $FRONTEND_IMAGE
docker run -d --network $NAMESPACE --network-alias redis redis:alpine
docker run -d --network $NAMESPACE \
  --network-alias box-db \
  -e POSTGRES_USER=redacted \
  -e POSTGRES_PASSWORD=redacted \
  -e POSTGRES_HOST=box-db \
  -e POSTGRES_DB=box-db \
  postgres:11-alpine

docker run -d --network $NAMESPACE \
  --name be-$NAMESPACE \
  -e BOX_URL=auth-service:5000 \
  -e BOX_API_KEY=qwerty \
  -e POSTGRES_USER=redacted \
  -e POSTGRES_PASSWORD=redacted \
  -e POSTGRES_HOST=box-db \
  -e POSTGRES_DB=box-db \
  -e REDIS_PORT=6379 \
  -e JWT_SECRET=$JWT_SECRET \
  $BACKEND_IMAGE	

docker run -d --network $NAMESPACE \
  -e BOX_URL=auth-service:5000 \
  -e BOX_API_KEY=qwerty \
  -e POSTGRES_USER=redacted \
  -e POSTGRES_PASSWORD=redacted \
  -e POSTGRES_HOST=box-db \
  -e POSTGRES_DB=box-db \
  -e REDIS_PORT=6379 \
  -e JWT_SECRET=$JWT_SECRET \
  $BACKEND_IMAGE celery worker \
        --app=app.worker.celery \
        --concurrency=20 \
        --loglevel=INFO

echo "> getting hostname"

FRONTEND_HOSTNAME=$(docker inspect fe-$NAMESPACE | jq '.[0].Config.Hostname' -r)
BACKEND_HOSTNAME=$(docker inspect be-$NAMESPACE | jq '.[0].Config.Hostname' -r)

echo "> register endpoint to redis"

# FIXME: for this demo i use lambda.store
redis-cli -u redis://9f6313d92d6e48bf8fc00f414de13f90@us1-sweet-orca-31794.lambda.store:31794 set fe:$ENDPOINT $FRONTEND_HOSTNAME
redis-cli -u redis://9f6313d92d6e48bf8fc00f414de13f90@us1-sweet-orca-31794.lambda.store:31794 set be:$ENDPOINT $BACKEND_HOSTNAME

exit $?
