#!/usr/bin/bash

set -eux

NAMESPACE=$1

echo "> cleaning up"

# yolo
docker rm $(docker ps -aq --filter "network=$NAMESPACE")

echo "> done"

exit $?
